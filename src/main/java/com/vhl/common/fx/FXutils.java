package com.vhl.common.fx;

import javafx.scene.control.Control;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;

/**
 * Created by Knight-en on 11/03/2017.
 */
public class FXutils {


    /**
     * Changes the field acceptable key inputs by excluding space
     * @param textField text field to be modified
     * @param charLimit input character limit
     */
    public static void noSpaceField(TextField textField, int charLimit) {
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.toCharArray().length > charLimit) {
                textField.setText(oldValue);
                return;
            }
            textField.setText(newValue.trim());
        });
    }

    /**
     * Changes the field acceptable key inputs by accepting only numeric values
     * @param textField text field to be modified
     * @param charLimit input char limit
     */
    public static void allNumericField(TextField textField, int charLimit) {
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("[*0-9]"))
                textField.setText(newValue.replaceAll("[^0-9]", ""));
            if (newValue.toCharArray().length > charLimit) {
                textField.setText(oldValue);
            }

        });

    }

    /**
     * Changes the field acceptable key inputs by accepting only alphanumeric values
     * @param textField text field to be modified
     * @param charLimit input char limit
     */
    public static void alphaNumericFields(TextField textField, int charLimit) {
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("[*a-zA-Z0-9]"))
                textField.setText(newValue.replaceAll("[^a-zA-Z0-9]", ""));
            if (newValue.toCharArray().length > charLimit) {
                textField.setText(oldValue);
            }
        });
    }

    /**
     * Set a tooltip for JavaFX controls
     * @param control
     * @param message
     */
    public static void setTooltip(Control control, String message) {
        Tooltip tooltip = new Tooltip();
        tooltip.setText(message);
        control.setTooltip(tooltip);
    }
}
