package com.vhl.common.utitities;

import java.util.HashMap;
import java.util.Map;

public class NumberSystem {

    private static Map<Character, Long> kmi;
    private static Map<Integer, String> kms;


    static {
        kms = new HashMap<>();
        kms.put(10, "A");
        kms.put(11, "B");
        kms.put(12, "C");
        kms.put(13, "D");
        kms.put(14, "E");
        kms.put(15, "F");

        kmi = new HashMap<>();
        kmi.put('A', (long) 10);
        kmi.put('B', (long) 11);
        kmi.put('C', (long) 12);
        kmi.put('D', (long) 13);
        kmi.put('E', (long) 14);
        kmi.put('F', (long) 15);
    }

    public static long convert(long decimal, int tobaseBelow10) {
        StringBuilder output = new StringBuilder();
        long rem = 0;
        while (decimal > 0) {
            rem = decimal % tobaseBelow10;
            output.append(rem);
            decimal /= tobaseBelow10;
        }
        output = output.reverse();
        return Integer.parseInt(output.toString());
    }

    public static String convertInString(int decimal, int base) {

        if (base == 10)
            return String.valueOf(convert(decimal, base));

        int rem = 0;
        StringBuilder output = new StringBuilder();

        while (decimal > 0) {
            rem = decimal % base;
            if (rem >= 10) {
                output.append(kms.get(rem));
            } else {
                output.append(rem);
            }
            decimal /= base;
        }
        return output.reverse().toString();
    }

    public static int convertToDecimal(String val, int base) {
        int output =0;
        char[] x = val.toCharArray();
        for (int i = 0; i <x.length ; i++) {
            if (kmi.get(x[i]) != null) {
                output += (int) Math.pow(base, x.length-1-i) * kmi.get(x[i]);
            } else {
                output += (int) Math.pow(base, x.length-1-i) * Integer.parseUnsignedInt(String.valueOf(x[i]));
            }
        }
        return output;
    }


}
