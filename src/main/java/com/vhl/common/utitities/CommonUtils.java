package com.vhl.common.utitities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @author blackmo/Knight-en - Victor Harlan D. Lacson
 */

public class CommonUtils {

    private static DateFormat dateFormat;
    public static Date formatDate(int year, int month, int day) {
        return (new GregorianCalendar(year, month, day).getTime());
    }

    public static Date formatTime(int hour, int minute) {
        Calendar time = new GregorianCalendar(0,0,0,hour,minute);
        return time.getTime();
    }

    public static String get12HourDateFormat(Date date) {
        dateFormat = new SimpleDateFormat("hh:mm a");
        return dateFormat.format(date);
    }

    /**
     * format date into mm/dd/yyyy
     * @param date
     * @return formatted value in String
     */
    public static String getSimpleDateFormat(Date date) {
        dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        return dateFormat.format(date);
    }

    /**
     * returns a converted value of LocalDate into Date
     * @param localDate
     * @return Date value
     */
    public static Date getDateFromLocalDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant()
        );
    }

    /**
     * convers a String representation of LocalDate value
     * @param localDate
     * @return String representation of LocalDate
     */
    public static String getSimpleDateFormat(LocalDate localDate) {
        return getSimpleDateFormat(getDateFromLocalDate(localDate));
    }

    /**
     * converts a specified Date object to a LocalDate format object
     * @param date to be converted object
     * @return converted LocalDate Object
     */
    public static LocalDate dateToLocalDate(Date date) {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }
}
