package com.vhl.common.utitities;


import org.apache.commons.io.*;

import java.io.*;

/**
 * @author blackmo/Knight-en - Victor Harlan D. Lacson
 */
public class TextFile {
    /**
     * Loads the content of a file into a string
     * @param path location of the file
     * @return the string representation of the content of the file
     */
    public static String openAStringFile(String path) {

        StringWriter writer = null;
        InputStream inputStream = null;
        String value = null;
        try {
            inputStream = new FileInputStream(path);
            writer = new StringWriter();
            IOUtils.copy(inputStream, writer, "UTF-8");
            value = writer.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return value;

        }
    }

    /**
     *
     * @param file_path
     * @return
     */
    public static String openAfile(String file_path) {

        StringWriter writer = null;
        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream(new File(file_path));
            writer = new StringWriter();
            IOUtils.copy(inputStream, writer, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return writer.toString();
    }
}
