package com.vhl.common.utitities;

import java.util.HashMap;
import java.util.Map;

/**
 * @author blackmo/Knight-en - Victor Harlan D. Lacson
 */
public class RomanConvertion {

    private static String roman;
    private static int occurrenceHolder;
    private static int val;
    private static char[] holder;
    private static Map<Integer, String> keyMapValue;
    private static Map<String, Integer> kmi;

    public static String integerToRomanNumerals(int val) {
        roman = "";
        atBetween(val, 1000);
        return roman;
    }

    static {
        roman = "";
        keyMapValue = new HashMap<Integer, String>();
        keyMapValue.put(1, "I");
        keyMapValue.put(5, "V");
        keyMapValue.put(10, "X");
        keyMapValue.put(50, "L");
        keyMapValue.put(100, "C");
        keyMapValue.put(500, "D");
        keyMapValue.put(1000, "M");

        kmi = new HashMap<>();
        kmi.put("I", 1);
        kmi.put("V", 5);
        kmi.put("X", 10);
        kmi.put("L", 50);
        kmi.put("C", 100);
        kmi.put("D", 500);
        kmi.put("M", 1000);
    }

    private static void atBetween(int val, int upper) {
        int x9s = upper - (upper / 10);
        int x5s = upper / 2;
        occurrenceHolder = val / upper; //getting the the occurrence of 1000
        for (int i = 0; i < occurrenceHolder; i++)
            roman += keyMapValue.get(upper);
            RomanConvertion.val = val % upper;
        if (upper > 1) at9s(x9s, x5s);
    }

    private static void at9s(int x9s, int x5s) {
        int newUpper = x9s / 9;
        if (val >= x9s) {
            roman += keyMapValue.get(newUpper) + keyMapValue.get(x9s + newUpper);
            val %= x9s; // getting remaining value less than x9s
            atBetween(val, newUpper);
        } else at5s(x5s);
    }

    private static void at5s(int x5s) {
        int x1s = (x5s / 5);
        int x4s = x5s - x1s;
        int remainder = val % x4s;
        occurrenceHolder = val / x4s;
        if (occurrenceHolder == 1 && remainder >= x1s) {
            roman += keyMapValue.get(x5s);
            val -= x5s; // getting values less than x5s
        } else if (occurrenceHolder == 1 && remainder < x1s) {
            roman += keyMapValue.get(x1s) + keyMapValue.get(x5s);
            val -= x4s; // getting value less than x4s
        }
        atBetween(val, x1s);
    }

    public static int romanToInteger(String val) {
        int output = 0;
        val = val.toUpperCase();
        holder = val.toCharArray();
        if (holder.length>1) {
            for (int i = 1; i < holder.length; i++) {
                if (toInteger(holder[i]) > toInteger(holder[i - 1])) {
                    output += toInteger(holder[i]) - toInteger(holder[i - 1]);
                } else output += toInteger(holder[i]);
            }
        } else return toInteger(holder[0]);

        if (holder.length > 1)
            if (toInteger(holder[0]) >= toInteger(holder[1])) {
                output += toInteger(holder[0]);
            }
        return output;
    }

    private static String charToString(char c) {
        return String.valueOf(c);
    }

    private static int toInteger(char c) {
        return kmi.get(charToString(c));
    }


    private static void atThousandOccurence() {
        System.out.println("@thousand");
        occurrenceHolder = (short) (val / 1000); // setting occurrence
        for (int i = 0; i < occurrenceHolder; i++) {
            roman += M;
        }
        val %= 1000; // getting remaining value less 1000
    }

    private static boolean at900Occurrence() {
        System.out.println("@900");
        if (val >= 900) {
            roman += C + M;
            val %= 900; // getting remaining value less than 900
            occurrenceHolder %= 100;
            at100ccurrence();
            return true;
        }
        return false;
    }

    private static void at500and400occurrence() {
        System.out.println("@500");
        occurrenceHolder = (short) (val / 400); //setting occurrence for 500 or 400
        if (occurrenceHolder >= 1) {
            roman += D;
            val -= 500;
        } else if (occurrenceHolder == 1) {
            System.out.println(occurrenceHolder);
            roman += C + D;
            val -= 400;
        }
        at100ccurrence();
    }

    private static void at100ccurrence() {
        System.out.println("@100");
        occurrenceHolder = (short) (val / 100);
        for (int i = 0; i < occurrenceHolder; i++) { // excluding first occurrence of 100 due to 500state
            roman += C;
        }
        val %= 100; // getting values below 100
        if (!at90occurrence())
            at50and40();
    }

    private static boolean at90occurrence() {
        System.out.println("@90");
        if (val >= 90) {
            roman += X + C;
            val %= 90;
            at10occurrence();
            return true;
        }
        return false;
    }

    private static void at50and40() {
        System.out.println("@50");
        occurrenceHolder = val / 40; //setting occurrence for 50 or 40
        if (occurrenceHolder > 1) {
            roman += L;
            val -= 50;
        } else if (occurrenceHolder == 1) {
            roman += X + L;
            val -= 40;
        }
        at10occurrence();
    }

    private static void at10occurrence() {
        System.out.println("@10");
        occurrenceHolder = (short) (val / 10);
        for (int i = 0; i < occurrenceHolder; i++) {
            roman += X;
        }
        val %= 10;
        if (!at9occurrence())
            at5occurrence();
    }

    private static boolean at9occurrence() {
        System.out.println("@9");
        if (val >= 9) {
            roman += I + X;
            return true;
        }
        return false;
    }

    private static void at5occurrence() {
        System.out.println("@5");
        occurrenceHolder /= 4;
        if (occurrenceHolder > 1) {
            System.out.println(occurrenceHolder);
            roman += V;
            val -= 5;
        } else if (occurrenceHolder == 1) {
            System.out.println(occurrenceHolder);
            roman += I + V;
            val -= 5;
        }
        at1occurrence();
    }

    private static void at1occurrence() {
        System.out.println("@1");
        if (val > 0) {
            for (int i = 0; i < val; i++) {
                roman += I;
            }
        }
    }

    private final static String M = "M";
    private final static String D = "D";
    private final static String C = "C";
    private final static String L = "L";
    private final static String X = "X";
    private final static String V = "V";
    private final static String I = "I";
}
