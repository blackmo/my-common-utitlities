package com.vhl.common.utitities;

import javafx.stage.FileChooser;
import javafx.stage.Window;

import java.io.File;

/**
 * @author blackmo/Knight-en - Victor Harlan D. Lacson
 */
public class JavaFXFileChooser {


    public static File previousDirectoryLocation;

    public static String loadATextFileFromPreviousDirectory(Window owner) {
        String string = null;
        File file = null;
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Load Model from File");
        if (previousDirectoryLocation != null)
            fileChooser.setInitialDirectory(previousDirectoryLocation.getAbsoluteFile());
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Load TextFile", "*txt", "*css", "*rc"));
        file = fileChooser.showOpenDialog(owner);
        if (file != null) {
            string = TextFile.openAStringFile(file.getPath());
        } else {
            System.out.println();
        }
        System.out.println(file.getPath());
        previousDirectoryLocation = file;
        return string;
    }

    public static String loadATextFile(Window owner) {
        String string = null;
        File file = null;
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Load Model from File");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Load TextFile", "*txt", "*css"));
        file = fileChooser.showOpenDialog(owner);
        if (file != null) {
            string = TextFile.openAStringFile(file.getPath());
        } else {
            System.out.println();
        }
        return string;
    }
}
